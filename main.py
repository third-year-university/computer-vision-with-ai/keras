import os.path
import numpy as np
from keras.models import Sequential, load_model
from keras.layers import Dense, Input, Dropout
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from keras.utils import to_categorical
from sklearn.metrics import classification_report
from collections import Counter


data = np.loadtxt("winequality-red.csv", skiprows=1, delimiter=',')


features = data[:, :-1]
labels = data[:, -1]

c = Counter(labels)
print(c)

print(np.unique(labels))

x_train, x_test, y_train, y_test = train_test_split(features, labels, test_size=0.2, random_state=42)

print(x_train.shape, x_test.shape)

print(np.min(features, axis=1))
print(np.max(features, axis=1))

scaler = MinMaxScaler(copy=True, feature_range=(0, 1))
scaler.fit(x_train)

scaled_x_train = scaler.transform(x_train)
scaled_x_test = scaler.transform(x_test)

print(scaled_x_train.min(), scaled_x_train.max())
print(scaled_x_test.min(), scaled_x_test.max())

min_label = np.min([np.unique(y_test).min(), np.unique(y_train).min()])

y_test = y_test - min_label
y_cat_test = to_categorical(y_test, len(np.unique(y_test)))
print(y_cat_test.shape)

y_train = y_train - min_label
y_cat_train = to_categorical(y_train, len(np.unique(y_train)))
print(y_cat_train.shape)


if not os.path.exists("model_wine.h5"):
    model = Sequential()
    model.add(Dense(30, input_dim=11, activation="relu"))
    model.add(Dense(200, activation="relu"))
    model.add(Dense(200, activation="relu"))
    model.add(Dense(400, activation="tanh"))
    model.add(Dense(6, activation="softmax"))

    model.summary()

    model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])

    model.fit(scaled_x_train, y_cat_train, epochs=200, batch_size=64, verbose=2, validation_data=(scaled_x_test, y_cat_test))
    model.save("model_wine.h5")
else:
    model = load_model("model_wine.h5")
    model.summary()

print(model.predict(scaled_x_test))

predicts = np.argmax(model.predict(scaled_x_test), 1)
print(predicts)

print(classification_report(y_test, predicts))


counter = 0
for i in range(len(y_test)):
    if y_test[i] == predicts[i]:
        counter+=1

print("accuracy:", counter / len(y_test))