import cv2
import numpy as np
from keras.models import Sequential, load_model

draw = False

model = load_model("my_model.h5")


def draw_callback(event, x, y, flags, param):
    global draw
    if event == cv2.EVENT_MOUSEMOVE:
        if draw:
            cv2.circle(img, (x, y), 10, 200, -1)
    elif event == cv2.EVENT_LBUTTONDOWN:
        draw = True
    elif event == cv2.EVENT_LBUTTONUP:
        draw = False


def init_img(image):
    cv2.rectangle(image, (255, 255), (768, 768), 200, 5)
    cv2.putText(image, 'ATTENTION! DRAW DIGIT STRICTLY INSIDE', (10, 100), cv2.FONT_HERSHEY_SIMPLEX,
                1, 200, 3, cv2.LINE_AA)
    cv2.putText(image, 'WHITE WINDOW AND PRESS M.', (10, 150), cv2.FONT_HERSHEY_SIMPLEX,
                1, 200, 3, cv2.LINE_AA)


img = np.zeros((1024, 1024), dtype="uint8")

cv2.namedWindow("MNIST")
cv2.setMouseCallback("MNIST", draw_callback)

init_img(img)

while True:
    cv2.imshow("MNIST", img)

    key = cv2.waitKey(1) & 0xFF
    if key == 27:
        break
    if key == ord('m'):
        print("RECOGNIZE")
        my_img = img[260:763, 260:763]
        my_img = cv2.resize(my_img, (28, 28), interpolation=cv2.INTER_AREA)
        print(my_img.shape)
        # cv2.imshow("MNIST2", my_img)
        img[800:828, 800:828] = my_img
        my_img = my_img / 255.
        to_deal = [my_img]
        to_deal = np.array(to_deal)
        to_deal = to_deal.reshape(*to_deal.shape, 1)
        result = model.predict(to_deal)
        prediction = np.argmax(result)
        cv2.putText(img, f'RESULT: {prediction}', (10, 800), cv2.FONT_HERSHEY_SIMPLEX,
                    1, 200, 3, cv2.LINE_AA)
        cv2.putText(img, f'PERCENTS: {np.round(result, 2)}', (10, 900), cv2.FONT_HERSHEY_SIMPLEX,
                    0.5, 200, 1, cv2.LINE_AA)


    if key == ord('c'):
        print("CLEAR")
        img[:] = 0
        init_img(img)

cv2.destroyAllWindows()
