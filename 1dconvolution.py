from keras.models import Sequential
from keras.layers import Conv1D
import numpy as np

data = np.array([0, 0, 0, 0, 1, 1, 1, 0, 0, 0])
data = data.reshape(1, 10, 1)

model = Sequential()
model.add(Conv1D(1, 2, input_shape=(10, 1)))

weights = [np.array([[[1]], [[-1]]]), np.array([0.0])]
model.set_weights(weights)
print(model.predict(data))